import os
from pysondb import db

DIR = os.getcwd()
PATH = f"{DIR}/maps"
DBDIR = f"{DIR}/DB"
DBPATH = f'{DIR}/DB/database.json'

dbOne = None

def init_db() -> None:
    if(os.path.exists(DBPATH) is False):
        if(does_dir_exist(DBDIR) is False):
            # Create directory to prevent connector error
            os.mkdir(DBDIR)
        
        dbOne = db.getDb(DBPATH)

    return

def does_dir_exist(path) -> bool:
    return os.path.isdir(path)

def get_maps_from_folder() -> None:
    return

def load_maps_to_storage(maps) -> None:
    return

def get_cwd() -> str:
    return DIR

# Initialize DB
init_db()