class Map:
    def __init__(self,data,column,row):
        self.data = data
        self.column = column
        self.row = row
        self.objects = []

    def get_data(self):
        return self.data

    def get_column(self):
        return self.column

    def get_row(self):
        return self.row

    def get_objects(self):
        return self.objects

    def change_object_positions(self, position):
        # Implement this once we have an object class with their own methods
        return
