from src.classes.map import Map

class Maps:
    def __init__(self):
        self.maps = []
        self.currMap = {"col": 1, "row": 1}
        return

    def goto_map(self, col, row):
        self.currMap.col = col
        self.currMap.row = row
        return

    def append_map(self, map: Map):
        self.maps.append(map)
        return
    
    def show_current_map(self):
        for i in self.maps:
            if(i.get_row() == self.currMap.row and i.get_column() == self.currMap.col):
                data = i.get_data()

                print(data)
                return
        
        print("No Map to show. Possibly incorrect column and/or row")
        return
