# Game Engine
A simple engine to create 2D terminal applications

# Features
- Simple DB for storing maps
- Easily create multiple rooms
- Create menus and sub menus dynamically
- Dynamically add objects and control each one

# Todo
- Add object class
- Add maps and possibly a map editor
- Add sub class support